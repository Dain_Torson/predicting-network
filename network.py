# -*- coding: utf-8 -*-
import numpy as np


class Network:
    """
    Нейронная сеть для предсказания числовых последовательностей
    """

    def __init__(self, layer1, layer2, activation=None, derivative=None):

        np.random.seed()
        self._weights1 = 2 * np.random.random((layer1 + 1, layer2)) - 1
        self._weights2 = 2 * np.random.random((layer2, 1)) - 1
        self._threshold1 = np.random.random((layer2, 1))
        self._threshold2 = np.random.random((1, 1))
        self._context = 0

        if activation is None or derivative is None:
            activation = lambda x: x
            derivative = lambda x: 1

        self._activation = np.vectorize(activation)
        self._derivative = np.vectorize(derivative)

    def feed(self, vec):
        vector = list(vec)
        vector.append(vector[-1])
        inputs = np.array(vector, ndmin=2)
        outputs1 = self._activation(np.dot(inputs, self._weights1) - self._threshold1.T)
        return np.dot(outputs1, self._weights2) - self._threshold2

    def calculate_error(self, inputs, standard):
        linear_err = self.feed(inputs) - standard
        return np.dot(np.ravel(linear_err), np.ravel(linear_err))

    def train_step(self, vec, standard, alpha):
        vector = list(vec)
        vector.append(self._context)

        inputs = np.array(vector, ndmin=2)
        outputs1 = self._activation(np.dot(inputs, self._weights1) - self._threshold1.T)
        outputs2 = np.dot(outputs1, self._weights2) - self._threshold2

        layer2_error = outputs2 - standard
        layer1_error = np.dot(layer2_error, self._weights2.T).T * self._derivative(outputs1).T

        weights1_delta = -alpha * np.dot(inputs.T, layer1_error.T)
        weights2_delta = -alpha * np.dot(outputs1.T, layer2_error)

        threshold1_delta = alpha * layer1_error
        threshold2_delta = alpha * layer2_error

        self._weights1 += weights1_delta
        self._weights2 += weights2_delta

        self._threshold1 += threshold1_delta
        self._threshold2 += threshold2_delta

        self._context = outputs2[0][0]

    def train(self, train_matrix, alpha, target_error):

        iterations = 0

        while True:

            for idx in range(len(train_matrix)):
                if idx + 1 < len(train_matrix):
                    self.train_step(train_matrix[idx],
                                    train_matrix[idx + 1][-1],
                                    alpha)

            self._context = 0

            error = 0

            for idx in range(len(train_matrix)):
                if idx + 1 < len(train_matrix):
                    error += self.calculate_error(train_matrix[idx], train_matrix[idx + 1][-1])

            if error < target_error:
                return error

            print("[" + str(iterations) + "] error " + str(error))
            iterations += 1

            if iterations > 100000:
                return error

