# -*- coding: utf-8 -*-
import math


def fib(n):
    a = 0
    b = 1
    for __ in range(n):
        a, b = b, a + b
    return a


def sigmoid(x):
    return 1 / (1 + math.exp(-x))


def sigmoid_derivative(sigm):
    return sigm*(1 - sigm)


def tanh_derivative(tanhx):
   return 1 - tanhx*tanhx


def tanh(x):
    return math.tanh(x)


def generator(n, m, func):
    return [[func(x) for x in range(idx, idx + m)] for idx in range(0, n)]


def factorial_matrix(n, m):
    return generator(n, m, math.factorial)


def pow_of_two_matrix(n, m):
    return generator(n, m, lambda x: math.pow(2, x))


def simple_periodic_matrix(n, m):
    return generator(n, m, lambda x: 1 if x % 4 == 0 else (-1 if x % 4 == 2 else 0))


def fibonacci_matrix(n, m):
    return generator(n, m, fib)

