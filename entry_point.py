# -*- coding: utf-8 -*-
import network as nw
import functions as func

generator = func.pow_of_two_matrix
activation = func.tanh
derivative = func.tanh_derivative

network = nw.Network(3, 4)
matrix = generator(4, 3)
print(matrix)

network.train(matrix, 0.0001, 0.05)
test = matrix[-1]
print(test)
print(network.feed(test))
print("Expected: " + str(generator(5, 3)[-1][-1]))

